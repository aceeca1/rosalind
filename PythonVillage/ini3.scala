object ini3 {
    import io.StdIn._

    def main(args: Array[String]) {
        val s = readLine
        val Array(a, b, c, d) = readLine.split(" ").map{_.toInt}
        println(s.substring(a, b + 1) + " " + s.substring(c, d + 1))
    }
}
