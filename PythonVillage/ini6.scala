object ini6 {
    import io.StdIn._
    import collection._

    def main(args: Array[String]) {
        val u = new mutable.HashMap[String, Int]
        for (i <- readLine.split(" "))
            u.get(i) match {
                case Some(ui) => u.update(i, ui + 1)
                case None => u.update(i, 1)
            }
        for ((k, v) <- u) println(k + " " + v)
    }
}
