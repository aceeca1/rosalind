object ini4 {
    import io.StdIn._

    def main(args: Array[String]) {
        val Array(a, b) = readLine.split(" ").map{_.toInt}
        println((a to b).filter{i => (i & 1) != 0}.sum)
    }
}
