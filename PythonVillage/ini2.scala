object ini2 {
    import io.StdIn._

    def main(args: Array[String]) {
        val Array(a, b) = readLine.split(" ").map{_.toInt}
        println(a * a + b * b)
    }
}
