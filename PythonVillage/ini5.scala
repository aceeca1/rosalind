object ini5 {
    import io.StdIn._

    def main(args: Array[String]) {
        var b = false
        while (true) {
            val s = readLine
            if (s == null) return
            if (b) println(s)
            b = !b
        }
    }
}
