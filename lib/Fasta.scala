package lib

case class DNA(name: String, content: String)

class Fasta(read: => String) extends Iterator[DNA] {
    import scala.annotation.tailrec

    var name: String = read.substring(1)
    val content = new StringBuilder

    def hasNext = name != null

    @tailrec final def next = {
        val line = read
        if (line == null || line.startsWith(">")) {
            val ans = DNA(name, content.toString)
            name = if (line == null) null else line.substring(1)
            content.clear
            ans
        } else {
            content.append(line)
            next
        }
    }
}
