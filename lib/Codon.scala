package lib

object Codon {
    val a = "FFLLSSSSYYZZCCZWLLLLPPPPHHQQRRRRIIIMTTTTNNKKSSRRVVVVAAAADDEEGGGG"

    def toNum(rna: Char) = rna match {
        case 'U' => 0
        case 'C' => 1
        case 'A' => 2
        case 'G' => 3
    }

    def toProtein(rna: String) =
        a((toNum(rna(0)) << 4) + (toNum(rna(1)) << 2) + toNum(rna(2)))
}
