object revc {
    import io.StdIn._

    def main(args: Array[String]) {
        println(readLine.reverseMap{
            case 'A' => 'T'
            case 'C' => 'G'
            case 'G' => 'C'
            case 'T' => 'A'
        })
    }
}
