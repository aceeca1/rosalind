object pper {
    import io.StdIn._

    def main(args: Array[String]) {
        val Array(n, k) = readLine.split(" ").map{_.toInt}
        println(Range(n, n - k, -1).reduce{_ * _ % 1000000})
    }
}
