object lexf {
    import io.StdIn._

    def main(args: Array[String]) {
        val a = readLine.split(" ")
        for (i <- Array.fill(readInt){a}.reduce{(c1, c2) =>
            for (i1 <- c1; i2 <- c2) yield i1 + i2
        }) println(i)
    }
}
