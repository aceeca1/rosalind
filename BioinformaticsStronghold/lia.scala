object lia {
    import io.StdIn._

    def f(m: Int, ans: Array[Double] = Array(1.0)): Array[Double] = {
        if (m == 0) return ans
        f(m - 1, Array.tabulate(ans.size + 1){i =>
            val p1 = if (i == 0) 1.0 else ans(i - 1)
            val p2 = if (i == ans.size) 0.0 else ans(i)
            0.25 * p1 + 0.75 * p2
        })
    }

    def main(args: Array[String]) {
        val Array(k, n) = readLine.split(" ").map{_.toInt}
        println(f(1 << k)(n))
    }
}
