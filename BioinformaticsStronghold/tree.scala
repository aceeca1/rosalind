object tree {
    import io.StdIn._

    def main(args: Array[String]) {
        val n = readInt
        var zLine = 0
        while (readLine != null) zLine += 1
        println(n - 1 - zLine)
    }
}
