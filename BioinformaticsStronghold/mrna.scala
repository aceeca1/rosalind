object mrna {
    import io.StdIn._
    import lib._

    def main(args: Array[String]) {
        val a = Array.ofDim[Int](256)
        for (i <- Codon.a) a(i) += 1
        println((readLine + "Z").map{a(_)}.reduce{_ * _ % 1000000})
    }
}
