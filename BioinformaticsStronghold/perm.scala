object perm {
    import io.StdIn._

    def main(args: Array[String]) {
        val n = readInt
        val a = Array.range(1, n + 1)
        println(a.product)
        for (i <- a.permutations) println(i.mkString(" "))
    }
}
