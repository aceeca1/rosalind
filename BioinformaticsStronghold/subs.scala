object subs {
    import io.StdIn._

    def main(args: Array[String]) {
        val s = readLine
        val a = s"(?=${readLine})".r.findAllMatchIn(s)
        println(a.map{_.start + 1}.mkString(" "))
    }
}
