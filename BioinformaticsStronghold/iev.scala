object iev {
    import io.StdIn._

    def main(args: Array[String]) {
        val a = readLine.split(" ").map{_.toDouble}
        val ans = a(0) + a(1) + a(2) + 0.75 * a(3) + 0.5 * a(4)
        println(ans * 2.0)
    }
}
