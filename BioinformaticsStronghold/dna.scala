object dna {
    import io.StdIn._

    def main(args: Array[String]) {
        val a = Array.ofDim[Int](4)
        for (i <- readLine) i match {
            case 'A' => a(0) += 1
            case 'C' => a(1) += 1
            case 'G' => a(2) += 1
            case 'T' => a(3) += 1
        }
        println(a.mkString(" "))
    }
}
