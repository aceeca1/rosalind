object rna {
    import io.StdIn._

    def main(args: Array[String]) {
        println(readLine.map{
            case 'T' => 'U'
            case si => si
        })
    }
}
