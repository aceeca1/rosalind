object lgis {
    import io.StdIn._
    import lib._

    def longestIncSubseq(a: Array[Int]) = {
        val b = Array.fill(a.size + 1){Int.MaxValue}
        val aFrom = Array.ofDim[Int](a.size)
        val bFrom = Array.ofDim[Int](b.size)
        b(0) = Int.MinValue
        bFrom(0) = -1
        for (i <- a.indices) {
            val k = BSearch(0, b.size){k => b(k) > a(i)}
            b(k) = a(i)
            bFrom(k) = i
            aFrom(i) = bFrom(k - 1)
        }
        val ans = BSearch(0, b.size){k => b(k) == Int.MaxValue} - 1
        val detail = Array.ofDim[Int](ans)
        detail(0) = bFrom(ans)
        for (i <- 1 until detail.size) detail(i) = aFrom(detail(i - 1))
        detail
    }

    def main(args: Array[String]) {
        val n = readInt
        val a = readLine.split(" ").map{_.toInt}
        val b = a.map{-_}
        println(longestIncSubseq(a).reverseMap(a).mkString(" "))
        println(longestIncSubseq(b).reverseMap(a).mkString(" "))
    }
}
