object orf {
    import io.StdIn._
    import lib._

    val reProt = "(?=(M[^Z]*)Z)".r

    def main(args: Array[String]) {
        val dna = new Fasta(readLine).next.content
        val rna = dna.replace('T', 'U')
        val rnaRC = rna.reverseMap{
            case 'A' => 'U'
            case 'C' => 'G'
            case 'G' => 'C'
            case 'U' => 'A'
        }
        val prots = for {
            i1 <- Iterator(rna, rnaRC)
            i2 <- Iterator(i1, i1.substring(1), i1.substring(2))
            rnas = i2.grouped(3).takeWhile{_.size == 3}
            prot = rnas.map{Codon.toProtein}.mkString
            i3 <- reProt.findAllMatchIn(prot).map{_.group(1)}
        } yield i3
        prots.toSet.foreach(println)
    }
}
