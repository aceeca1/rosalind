object prot {
    import io.StdIn._
    import lib._

    def main(args: Array[String]) {
        val a = readLine.grouped(3).map(Codon.toProtein)
        println(a.takeWhile{_ != 'Z'}.mkString)
    }
}
