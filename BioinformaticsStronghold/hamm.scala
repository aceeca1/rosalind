object hamm {
    import io.StdIn._

    def main(args: Array[String]) {
        val s1 = readLine
        val s2 = readLine
        println(s1.indices.count{i => s1(i) != s2(i)})
    }
}
