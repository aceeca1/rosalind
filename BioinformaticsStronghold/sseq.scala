object sseq {
    import io.StdIn._
    import lib._

    def main(args: Array[String]) {
        val fasta = new Fasta(readLine)
        val s1 = fasta.next.content
        val s2 = fasta.next.content
        val a = Array.ofDim[Int](s2.size)
        var k = 0
        println((0 until s2.size).map{i =>
            while (s1(k) != s2(i)) k += 1
            k += 1
            k
        }.mkString(" "))
    }
}
