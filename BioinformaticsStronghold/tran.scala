object tran {
    import io.StdIn._
    import lib._

    def main(args: Array[String]) {
        val fasta = new Fasta(readLine)
        val s1 = fasta.next.content
        val s2 = fasta.next.content
        var transition = 0
        var transversion = 0
        for (i <- 0 until s1.size)
            if ((s1(i) match {
                case 'A' => 'G'
                case 'C' => 'T'
                case 'G' => 'A'
                case 'T' => 'C'
            }) == s2(i)) transition += 1
            else if (s1(i) != s2(i)) transversion += 1
        println(transition.toDouble / transversion)
    }
}
