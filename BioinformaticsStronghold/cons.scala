object cons {
    import io.StdIn._
    import lib._

    def main(args: Array[String]) {
        val fasta = new Fasta(readLine).toBuffer
        val a = fasta.head.content.indices.map{i =>
            val ans = Array.ofDim[Int](4)
            for (DNA(_, content) <- fasta) content(i) match {
                case 'A' => ans(0) += 1
                case 'C' => ans(1) += 1
                case 'G' => ans(2) += 1
                case 'T' => ans(3) += 1
            }
            ans
        }
        println(a.map{ai => "ACGT"((0 until 4).maxBy(ai))}.mkString)
        for (i <- 0 until 4)
            println("ACGT"(i) + ": " + a.map{_(i)}.mkString(" "))
    }
}
