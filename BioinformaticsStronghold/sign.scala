object sign {
    import io.StdIn._

    def printSign(a: Array[Int], m: Int) {
        if (m == a.size) return println(a.mkString(" "))
        printSign(a, m + 1)
        a(m) = -a(m)
        printSign(a, m + 1)
        a(m) = -a(m)
    }

    def main(args: Array[String]) {
        val n = readInt
        val a = Array.range(1, n + 1)
        println(a.product << n)
        for (i <- a.permutations) printSign(i, 0)
    }
}
