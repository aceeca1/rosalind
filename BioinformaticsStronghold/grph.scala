object grph {
    import io.StdIn._
    import lib._

    def main(args: Array[String]) {
        val fasta = new Fasta(readLine).toBuffer
        val a = fasta.groupBy{_.content.take(3)}
        for (dna1 <- fasta) a.get(dna1.content.takeRight(3)) match {
            case Some(ai) =>
                for (dna2 <- ai)
                    if (dna1 != dna2) println(dna1.name + " " + dna2.name)
            case None => ()
        }
    }
}
