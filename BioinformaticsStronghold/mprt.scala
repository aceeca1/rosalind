object mprt {
    import io.StdIn._
    import lib._

    val nGlycosylation = "(?=N[^P][ST][^P])".r

    def main(args: Array[String]) {
        while (true) {
            val line = readLine
            if (line == null) return
            val url = s"http://www.uniprot.org/uniprot/$line.fasta"
            val lines = io.Source.fromURL(url).getLines
            val protein = new Fasta(
                if (lines.hasNext) lines.next else null
            ).next.content
            val matches = nGlycosylation.findAllMatchIn(protein)
            val ans = matches.map{_.start + 1}.mkString(" ")
            if (ans.nonEmpty) {
                println(line)
                println(ans)
            }
        }
    }
}
