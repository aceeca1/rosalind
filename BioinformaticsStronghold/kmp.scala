object kmp {
    import io.StdIn._
    import lib._

    def main(args: Array[String]) {
        val s = new Fasta(readLine).next.content
        val a = Array.ofDim[Int](s.size + 1)
        a(0) = -1
        for (i <- 1 to s.size) {
            var k = a(i - 1)
            while (k >= 0 && s(k) != s(i - 1)) k = a(k)
            a(i) = k + 1
        }
        println(a.tail.mkString(" "))
    }
}
