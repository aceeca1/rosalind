object gc {
    import io.StdIn._
    import lib._

    def main(args: Array[String]) {
        val fasta = new Fasta(readLine)
        def gcRatio(dna: DNA) =
            dna.content.count{i => i == 'G' || i == 'C'}.toDouble /
            dna.content.size
        val m = fasta.maxBy(gcRatio)
        println(m.name)
        println(gcRatio(m) * 100)
    }
}
