object fib {
    import io.StdIn._

    class FibIter(k: Int) extends Iterator[Long] {
        var a0 = 1L
        var a1 = 1L

        def hasNext = true
        def next = {
            val a2 = a1
            a1 = a0
            a0 = a1 + k * a2
            a2
        }
    }

    def main(args: Array[String]) {
        val Array(n, k) = readLine.split(" ").map{_.toInt}
        println(new FibIter(k).drop(n - 1).next)
    }
}
