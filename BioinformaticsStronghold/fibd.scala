object fibd {
    import io.StdIn._
    import collection._

    class Wabbit(m: Int) extends Iterator[Long] {
        val a = new mutable.Queue[Long]
        for (i <- 1 until m) a.enqueue(0L)
        a.enqueue(1L)
        var sum = 1L
        var last = 1L
        def hasNext = true

        def next = {
            val ans = sum
            val born = sum - last
            a.enqueue(born)
            last = born
            sum = sum + born - a.dequeue
            ans
        }
    }

    def main(args: Array[String]) {
        val Array(n, m) = readLine.split(" ").map{_.toInt}
        println(new Wabbit(m).drop(n - 1).next)
    }
}
