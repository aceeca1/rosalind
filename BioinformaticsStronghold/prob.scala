object prob {
    import io.StdIn._

    def main(args: Array[String]) {
        val s = readLine
        val zCG = s.count{i => i == 'C' || i == 'G'}
        val zAT = s.size - zCG
        println(readLine.split(" ").map{i =>
            val n = i.toDouble
            val logProbCG = Math.log10(0.5 * n)
            val logProbAT = Math.log10(0.5 * (1.0 - n))
            logProbCG * zCG + logProbAT * zAT
        }.mkString(" "))
    }
}
