object lcsm {
    import io.StdIn._
    import lib._

    def solve: String = {
        val fasta = new Fasta(readLine).toBuffer
        val s1 = fasta.head.content
        for {
            iZ <- Range(s1.size, -1, -1)
            iS <- 0 until s1.size - iZ
            iT = iS + iZ
            i = s1.substring(iS, iT)
            if (fasta.forall{_.content.contains(i)})
        } return i; null
    }

    def main(args: Array[String]) {
        println(solve)
    }
}
