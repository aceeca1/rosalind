object splc {
    import io.StdIn._
    import lib._

    def main(args: Array[String]) {
        val fasta = new Fasta(readLine)
        var all = fasta.next.content
        for (i <- fasta) all = all.replaceAll(i.content, "")
        val rnas = all.replace('T', 'U').grouped(3)
        val protein = rnas.map(Codon.toProtein).takeWhile{_ != 'Z'}
        println(protein.mkString)
    }
}
