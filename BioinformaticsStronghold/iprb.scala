object iprb {
    import io.StdIn._

    def main(args: Array[String]) {
        val Array(k, m, n) = readLine.split(" ").map{_.toDouble}
        val population = k + m + n
        val allCases = population * (population - 1.0)
        val recessiveCases = 0.25 * m * (m - 1.0) + m * n + n * (n - 1.0)
        val dominantCases = allCases - recessiveCases
        println(dominantCases / allCases)
    }
}
