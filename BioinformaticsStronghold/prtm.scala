object prtm {
    import io.StdIn._
    import lib._

    def main(args: Array[String]) {
        println(readLine.map{Mass.a(_)}.sum)
    }
}
