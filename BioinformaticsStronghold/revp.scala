object revp {
    import io.StdIn._
    import lib._

    def main(args: Array[String]) {
        val dna = new Fasta(readLine).next.content
        for {
            len <- 4 to 12
            iS <- 0 to dna.size - len
            iT = iS + len
            s1 = dna.substring(iS, iT)
            s2 = s1.reverseMap{
                case 'A' => 'T'
                case 'C' => 'G'
                case 'G' => 'C'
                case 'T' => 'A'
            }
            if (s1 == s2)
        } println(s"${iS + 1} $len")
    }
}
