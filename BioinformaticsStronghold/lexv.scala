object lexv {
    import io.StdIn._

    def main(args: Array[String]) {
        val a = readLine.split(" ")
        val n = readInt
        def put(s: String) {
            if (s.nonEmpty) println(s)
            if (s.size == n) return
            for (i <- a) put(s + i)
        }
        put("")
    }
}
